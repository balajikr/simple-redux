import { createStore } from "redux";
import registerReducer from "../Reducers/register";


const store = createStore(registerReducer);

store.subscribe(() => console.log(store.getState()));
/* console.log("redux store", store.getState()); */

export default store;
