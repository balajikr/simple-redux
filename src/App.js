import React, { useState, Component } from "react";
import "./App.css";
import Login from "./components/Login/Login";
import Register from "./components/Register/Register";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./Store";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Provider store={store}>
          <Router>
            <Switch>
              <Route exact path="/" component={Login} />
              <Route path="/register" component={Register} />
            </Switch>
          </Router>
        </Provider>
      </div>
    );
  }
}

export default App;
