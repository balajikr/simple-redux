import React from "react";

//StateFull Component
class Login extends React.Component {
  /**
   * Defining state
   */
  state = {
    fields: {
      username: "",
      password: ""
    }
  };

  /**
   *
   * @param {*} event
   *
   * @return state
   */
  handleFieldsChange = e => {
    e.preventDefault();
    const { fields } = this.state;
    fields[e.target.name] = e.target.value;
    this.setState({ fields });
  };

  /**
   *
   * @param {*} event
   *
   * @return state
   */

  handleSubmit = e => {
    e.preventDefault();
    console.log("loginCredentials", this.state.fields);
    this.props.history.push("/register")
  };

  //render HTML JSX Element
  render() {
    const { fields } = this.state;
    return (
      <div className="App">
        <form onSubmit={e => this.handleSubmit(e)}>
          <h2>Login</h2>
          <label>Username: </label>
          <input
            type="text"
            name="username"
            value={fields.username}
            onChange={e => this.handleFieldsChange(e)}
          />
          <br />
          <br />

          <label>Password: </label>
          <input
            type="password"
            name="password"
            value={fields.password}
            onChange={e => this.handleFieldsChange(e)}
          />
          <br />
          <br />

          <button>Submit</button>
        </form>
      </div>
    );
  }
}

export default Login;
