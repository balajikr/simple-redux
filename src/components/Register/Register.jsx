import React from "react";
import DisplayRegister from "./DisplayRegister";
import { connect } from "react-redux";
import { getRegister } from "../../actions/register";

//StateFull Component
class Register extends React.Component {
  /**
   * Defining state
   */
  state = {
    fields: {
      firstname: "",
      lastname: "",
      email: "",
      password: "",
      confirmpassword: ""
    }
  };

  /**
   *
   * @param {*} event
   *
   * @return state
   */
  handleFieldsChange = e => {
    e.preventDefault();
    const { fields } = this.state;
    fields[e.target.name] = e.target.value;
    this.setState({ fields });
  };

  /**
   *
   * @param {*} event
   *
   * @return state
   */

  handleSubmit = e => {
    e.preventDefault();
    //console.log("loginCredentials", this.state.fields);
    this.props.sendRegisterData(this.state.fields);
  };

  //render HTML JSX Element
  render() {
    //console.log("Registercomponent", this.props);
    const { fields } = this.state;
    return (
      <div className="App">
        <form onSubmit={e => this.handleSubmit(e)}>
          <h2>Register</h2>
          <label>FirstName: </label>
          <input
            type="text"
            name="firstname"
            value={fields.firstname}
            onChange={e => this.handleFieldsChange(e)}
          />
          <br />
          <br />

          <label>LastName: </label>
          <input
            type="text"
            name="lastname"
            value={fields.lastname}
            onChange={e => this.handleFieldsChange(e)}
          />
          <br />
          <br />

          <label>Email: </label>
          <input
            type="text"
            name="email"
            value={fields.email}
            onChange={e => this.handleFieldsChange(e)}
          />
          <br />
          <br />

          <label>Password: </label>
          <input
            type="password"
            name="password"
            value={fields.password}
            onChange={e => this.handleFieldsChange(e)}
          />
          <br />
          <br />

          <label>Confirm Password: </label>
          <input
            type="password"
            name="confirmpassword"
            value={fields.confirmpassword}
            onChange={e => this.handleFieldsChange(e)}
          />
          <br />
          <br />

          <button>Submit</button>
        </form>
        <DisplayRegister {...this.props} />
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    sendRegisterData: data => dispatch(getRegister(data))
  };
};

export default connect(null, mapDispatchToProps)(Register);
