import React, { Component, Fragment } from "react";
import { connect } from "react-redux";

class DisplayRegister extends Component {
  render() {
    return (
      <Fragment>
        <h3>Register Component </h3>
        {Object.entries(this.props.registerVal).map(([key, value]) => {
          return (
            <table>
              <tr>
                <td>{key}</td>
                <td>{value}</td>
              </tr>
            </table>
          );
        })}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    registerVal: state.register
  };
};

export default connect(mapStateToProps)(DisplayRegister);
