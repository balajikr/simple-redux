import { REGISTER } from "../Constants";

export const getRegister = payload => {
  return {
    type: REGISTER,
    payload
  };
};
