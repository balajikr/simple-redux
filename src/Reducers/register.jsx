import { REGISTER } from "../Constants";

const initialState = {
  register: {}
};

const registerReducer = (state = initialState, action) => {
  switch (action.type) {
    case REGISTER:
      return Object.assign({}, state, { register: { ...action.payload } });
    default:
      return state;
  }
};

export default registerReducer;
